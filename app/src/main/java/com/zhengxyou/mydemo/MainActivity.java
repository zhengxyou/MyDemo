package com.zhengxyou.mydemo;

import android.os.Bundle;
import android.view.View;

import com.zhengxyou.mydemo.json.JsonActivity;
import com.zhengxyou.mydemo.lottie.LottieActivity;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btn_lottie).setOnClickListener(this);
        findViewById(R.id.btn_Json).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_Json:
                JsonActivity.start(this);
                break;
            case R.id.btn_lottie:
                LottieActivity.start(this);
                break;
        }
    }
}
