package com.zhengxyou.mydemo.lottie;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.LottieCompositionFactory;
import com.airbnb.lottie.LottieListener;
import com.airbnb.lottie.LottieTask;
import com.zhengxyou.mydemo.R;

import androidx.appcompat.app.AppCompatActivity;

/**
 * 翻译@see https://zhuanlan.zhihu.com/p/25855085
 * https://blog.csdn.net/qq_15538877/article/details/80503773
 *
 * https://www.jianshu.com/p/cae606f45c0b
 * https://www.jianshu.com/p/282d098cf928?utm_source=oschina-app
 * https://juejin.im/post/5a30a10a6fb9a0451e3fc909
 * https://juejin.im/post/5a30a2d9f265da430e4f250d
 * https://blog.csdn.net/vitamio/article/details/70046998
 */
public class LottieActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lottie);

        //第2种，不在布局文件中设置
        LottieAnimationView animationView = findViewById(R.id.animation_view);
        final LottieAnimationView animation_view2 = findViewById(R.id.animation_view2);
        final LottieAnimationView animation_view3 = findViewById(R.id.animation_view3);
        final LottieAnimationView animation_view4 = findViewById(R.id.animation_view3);
        animation_view3.setAnimation("72-favourite-app-icon.json");
        animation_view3.loop(true);
        animation_view3.playAnimation();
        animation_view3.addAnimatorUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
//                Log.e("TAG", "progress:" + animation.getAnimatedValue());
            }
        });
        animation_view3.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        LottieTask<LottieComposition> lottieCompositionLottieTask = LottieCompositionFactory.fromAsset(this, "1798-check-animation.json");
        LottieListener<LottieComposition> compositionLottieListener = new LottieListener<LottieComposition>() {
            @Override
            public void onResult(LottieComposition result) {
                animation_view2.setComposition(result);
                animation_view2.playAnimation();
            }
        };
        lottieCompositionLottieTask.addListener(compositionLottieListener);

        ValueAnimator animator = ObjectAnimator.ofFloat(0f, 1f).setDuration(500);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                animation_view4.setProgress(Float.parseFloat(animation.getAnimatedValue().toString()));
            }
        });

    }

    public static void start(Context context) {
        Intent starter = new Intent(context, LottieActivity.class);
        context.startActivity(starter);
    }
}
