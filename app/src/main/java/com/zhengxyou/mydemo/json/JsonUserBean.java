package com.zhengxyou.mydemo.json;

public class JsonUserBean {
    public String firstName;
    public String lastName;

    public JsonUserBean(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
