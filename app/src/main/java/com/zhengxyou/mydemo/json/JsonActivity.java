package com.zhengxyou.mydemo.json;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.zhengxyou.mydemo.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;

/**
 * 参考@see https://www.cnblogs.com/cheneasternsun/p/5444234.html
 */
public class JsonActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json);

        testJsonArray();
        testJSONStringer();
        testJSONObject();
        testJSONTokener();
    }

    private void testJSONTokener() {

        String json = "{"
                + "  \"query\": \"Pizza\", "
                + "  \"locations\": [ 94043, 90210 ] "
                + "}";
        String arrrayJson = "[ 94043, 90210 ]";

        JSONTokener jsonTokener = new JSONTokener(json);
        JSONTokener arrayTokener = new JSONTokener(arrrayJson);

        checkObject(arrayTokener);
        checkObject(jsonTokener);

    }

    private void checkObject(JSONTokener jsonTokener) {
        try {
            Object o = jsonTokener.nextValue();
            if (o instanceof JSONObject) {
                Log.e("TAG", "JSONObject");
            } else if (o instanceof JSONArray) {
                Log.e("TAG", "JSONArray");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void testJSONObject() {
        Map<String, String> map = new HashMap<>();
        map.put("a", "A");
        map.put("b", "B");
        map.put("c", "C");
        JSONObject jsonObject = new JSONObject(map);
        Log.e("TAG", jsonObject.toString());
        try {
            jsonObject.put("d", "D");
            jsonObject.put("e", 5);
            jsonObject.put("f", true);

            List<Integer> list = new ArrayList<>();
            for (int i = 0; i < 6; i++) {
                list.add(i);
            }
            JSONArray array = new JSONArray(list);
            jsonObject.put("list", array);

            JSONObject object = new JSONObject();
            object.put("name", "张三");
            object.put("age", 33);
            jsonObject.put("user", object);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("TAG", jsonObject.toString());
        testReadJson(jsonObject.toString());
    }

    private void testReadJson(String toString) {
        try {

            JSONObject readJson = new JSONObject(toString);
            JSONArray list = readJson.getJSONArray("list");
            Log.e("TAG", "array===");
            for (int i = 0; i < list.length(); i++) {
                Log.e("TAG", String.valueOf(i));
            }
            Log.e("TAG", "array end===");

            Log.e("TAG", readJson.getString("a"));
            Log.e("TAG", String.valueOf(readJson.getInt("e")));
            Log.e("TAG", String.valueOf(readJson.getBoolean("f")));
            JSONObject user = readJson.getJSONObject("user");
            Log.e("TAG", user.getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void testJSONStringer() {
        JSONStringer jsonStringer = new JSONStringer();
        try {
            jsonStringer.object();
            jsonStringer.key("a");
            jsonStringer.value("1");
            jsonStringer.key("b");
            jsonStringer.value(1);
            jsonStringer.key("c");
            jsonStringer.value(false);

            jsonStringer.key("user");
            jsonStringer.object();
            jsonStringer.key("name").value("张三");
            jsonStringer.key("age").value("2223");
            jsonStringer.endObject();

            jsonStringer.key("array");
            jsonStringer.array();
            jsonStringer.value(1);
            jsonStringer.value(2);
            jsonStringer.value(3);
            jsonStringer.value(4);
            jsonStringer.value(5).value(6);
            jsonStringer.endArray();
            jsonStringer.endObject();
            Log.e("TAG", jsonStringer.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void testJsonArray() {
        JSONArray array = new JSONArray();
        for (int i = 0; i < 6; i++) {
//            array.put(i);
            array.put(String.valueOf(i));
//            array.put(2L);
//            array.put(false);
//            array.put("abc");
        }
        Log.e("TAG", array.toString());
    }

    public static void start(Context context) {
        Intent starter = new Intent(context, JsonActivity.class);
        context.startActivity(starter);
    }
}
